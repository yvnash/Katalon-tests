<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_z rachunku                             _3cda59</name>
   <tag></tag>
   <elementGuidId>8dfc96fe-57df-42ca-9df2-6a862c881302</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='transfer_new_out']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.form-fields</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-fields</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                        
                                            z rachunku
                                            
                                                [KO] konto na życie [13 159,20 PLN] 4141...0000
                                                    [KO] konto na życie [13 159,20 PLN] 4141...0000
                                                
                                            
                                            
                                                
                                                    
                                                        dostępne środki:
                                                        
                                                            13 159,20 PLN
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            nazwa odbiorcy
                                            
                                                
                                                    
                                                        
                                                    
                                                    
                                                        
                                                    
                                                    
                                                    
                                                
                                            
                                        
                                        
                                            na rachunek
                                            
                                                
                                            
                                        
                                        
                                            adres (opcjonalnie)
                                            
                                                
                                            
                                            
                                                
                                                adres (opcjonalnie)
                                            
                                        
                                        
                                            
                                                
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                
                                            
                                        
                                        
                                            kwota
                                            
                                                
                                            
                                            
                                                PLN
                                            
                                            
                                                
                                                    
                                                        po przelewie:
                                                        
                                                            13 159,20 PLN
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            tytułem
                                            
                                                 przelew środków
                                            
                                            
                                                zostało znaków: 125
                                            
                                        
                                        
                                            data realizacji
                                            
                                                
                                            
                                            
                                                
                                            
                                        
                                        
                                            
                                                
                                                    
                                                    zwykły
                                                
                                                
                                                    
                                                        
                                                        ekspresowy
                                                    
                                                    
                                                        
                                                            Przelew realizowany nawet w 15 minut za pośrednictwem operatora Blue Media S.A.
                                                        
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        kwota prowizji:
                                                        
                                                            0,00 PLN
                                                        
                                                    
                                                
                                            
                                        
                                        
                                            
                                                
                                                    
                                                    potwierdzenie e-mail
                                                
                                            
                                        
                                        
                                            adres e-mail
                                            
                                                
                                            
                                        
                                        
                                            
                                                
                                                    
                                                    dodaj do listy odbiorców
                                                
                                                
                                                    
                                                        Możesz zapisać odbiorcę do &quot;listy odbiorców&quot;. Odbiorca może zostać również zapisany na liście jako &quot;zaufany&quot; dzięki czemu następne przelewy nie będą wymagały dodatkowej autoryzacji.
                                                    
                                                
                                            
                                        
                                        
                                            
                                                pod nazwą
                                                
                                                    
                                                
                                            
                                            
                                                
                                                    
                                                        
                                                        jako zaufanego
                                                    
                                                
                                            
                                        
                                    </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;transfer_new_out&quot;)/div[@class=&quot;form-fields&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='transfer_new_out']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='dowolny'])[1]/following::div[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='przelew dowolny'])[1]/following::div[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div</value>
   </webElementXpaths>
</WebElementEntity>
