<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_identyfikator                          _945a18</name>
   <tag></tag>
   <elementGuidId>de1d6e5c-9667-40be-878c-f26f1d28d93b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='login_form']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.fieldset</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>fieldset</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                
                                    identyfikator
                                    
                                        
                                    pole wymagane
                                    
                                        
                                    
                                    
                                        
                                            
                                                
                                                    Wprowadź identyfikator otrzymany z banku lub alias - dodatkowy
                                                    własny identyfikator, samodzielnie zdefiniowany w demobank online.
                                                
                                            
                                        
                                    
                                    
                                        przypomnij identyfikator
                                    
                                
                                
                                    hasło
                                    
                                        
                                        utraciłem hasło
                                    
                                    
                                        
                                    
                                    
                                        
                                            
                                                
                                                    Wprowadź swoje hasło. Sprawdź, czy przycisk Caps Lock jest
                                                    włączony. Uwaga: 3-krotne wprowadzenie błędnego hasła spowoduje
                                                    zablokowanie dostępu do systemu.
                                                
                                            
                                        
                                    
                                
                                
                                    token
                                    
                                        
                                    
                                    
                                        
                                    
                                    
                                        
                                            
                                                
                                                    Wpisz aktualne wskazanie tokena.
                                                
                                            
                                        
                                    
                                
                                
                                    dalej
                                
                            </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login_form&quot;)/div[@class=&quot;fieldset&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login_form']/div</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='demobank w sam raz do testów'])[1]/following::div[6]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div</value>
   </webElementXpaths>
</WebElementEntity>
