import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')


WebUI.navigateToUrl('https://demobank.jaktestowac.pl/logowanie_etap_1.html')
//asercja czy pojawia sie identyfikator wymagane
WebUI.click(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Logowanie/div_identyfikator                          _945a18'))
WebUI.click(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Logowanie/input_identyfikator_login_id'))
WebUI.click(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Logowanie/div_identyfikator                          _945a18'))
WebUI.verifyElementPresent(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Logowanie/div_pole wymagane'),
	0)

//podanie ID
WebUI.setText(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Logowanie/input_identyfikator_login_id'), 
    userId)
WebUI.click(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Logowanie/button_dalej'))

// logowanie sie do serwisu
WebUI.setText(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Logowanie/input_haso_haslo'), 
    userPassword)

WebUI.click(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Logowanie/button_zaloguj si'))

//sprawddzenie czy uzytkownik jest tym ktorego chcemy 
WebUI.verifyElementText(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Pulpit/a_Jan Demobankowy'), 
    userFirstname + " " + userLastname)

WebUI.selectOptionByValue(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Pulpit/select_wybierz odbiorc przelewu            _029a71'),
	listUserNo, true)

WebUI.setText(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Pulpit/input_kwota_widget_1_transfer_amount'),
	quickTransferAmount)

WebUI.setText(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Pulpit/input_tytuem_widget_1_transfer_title'),
	quickTransferSubject)


WebUI.selectOptionByValue(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Pulpit/select_wybierz telefon do doadowania       _5c7a19'),
	'500 xxx xxx', true)

WebUI.setText(findTestObject('Object Repository/Test Logowanie/Page_Demobank - Bankowo Internetowa - Pulpit/input_kwota_widget_1_topup_amount'),
	topUpAmount)

