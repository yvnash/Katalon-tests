import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://demobank.jaktestowac.pl/pulpit.html')

WebUI.click(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Pulpit/a_patnoci'))

WebUI.click(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/div_z rachunku                             _3cda59'))

WebUI.setText(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/input_nazwa odbiorcy_form_receiver'), 
    receiver)

WebUI.setText(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/input_na rachunek_form_account_to'), 
    accountNo)

WebUI.click(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/span_adres (opcjonalnie)_i-show showhide'))

WebUI.setText(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/input_adres (opcjonalnie)_form_receiver_address1'), 
    'Kuala Lumpur')

WebUI.setText(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/input_adres (opcjonalnie)_form_receiver_address2'), 
    '21-115 Karbonara')

WebUI.setText(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/input_adres (opcjonalnie)_form_receiver_address3'), 
    'Ul. Papajska 37')

WebUI.setText(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/input_kwota_form_amount'), 
    amount.toString())
WebUI.verifyEqual((availableAmount - WebUI.getText(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/KwotaPoPrzlewie')).replace(' ', '').replace(',', '.').toDouble()).round(2), amount)

WebUI.setText(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/textarea_przelew rodkw'), 
    subject)

WebUI.click(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/input_data realizacji_form_date'))

WebUI.click(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/a_17'))

WebUI.click(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/span'))

WebUI.setText(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/input_adres e-mail_form_email'), 
    email)


WebUI.click(findTestObject('Object Repository/Test Przelew/Page_Demobank - Bankowo Internetowa - Przelew/button_dalej'))

